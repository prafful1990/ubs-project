package com.ubs.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.ubs.controller.CompanyController;
import org.ubs.model.EmployeeResponse;
import org.ubs.service.CompanyService;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CompanyControllerTest {

    @InjectMocks
    private CompanyController controller;

    @Mock
    private CompanyService service;

    @Test
    public void testGetEmployees() {
        String companyName = "ABC";
        EmployeeResponse employeeResponse = Mockito.mock(EmployeeResponse.class);
        Mockito.when(service.getEmployees(companyName)).thenReturn(employeeResponse);
        EmployeeResponse response = controller.getEmployees(companyName);
        assertNotNull(response);
    }
}
