package com.ubs.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.ubs.controller.CompanyController;
import org.ubs.model.EmployeeResponse;
import org.ubs.service.CompanyServiceImpl;
import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
public class CompanyServiceImplTest {

    @InjectMocks
    private CompanyServiceImpl service;

    @Mock
    private RestTemplate restTemplate;

    @Test
    public void testGetEmployees() {
        String companyName = "ABC";
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("companyName", companyName);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<EmployeeResponse> responseEntity = Mockito.mock(ResponseEntity.class);
        EmployeeResponse employeeResponse = Mockito.mock(EmployeeResponse.class);
        Mockito.when(restTemplate.postForEntity("http://localhost:9091/employee/details",request,EmployeeResponse.class)).thenReturn(responseEntity);
        Mockito.when(responseEntity.getBody()).thenReturn(employeeResponse);
        EmployeeResponse response = service.getEmployees(companyName);
        assertNotNull(response);
    }
}
