package org.ubs.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EmployeeResponse {
    private List<EmployeeDetails> employees;
}
