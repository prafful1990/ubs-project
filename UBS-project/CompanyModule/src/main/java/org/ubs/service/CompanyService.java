package org.ubs.service;

import org.ubs.model.EmployeeDetails;
import org.ubs.model.EmployeeResponse;

import java.util.List;

public interface CompanyService {
    EmployeeResponse getEmployees(String companyName);
}
