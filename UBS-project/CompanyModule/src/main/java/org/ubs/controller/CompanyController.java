package org.ubs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.ubs.model.EmployeeResponse;
import org.ubs.service.CompanyService;

@RestController
@RequestMapping("company")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @GetMapping("/employees")
    public EmployeeResponse getEmployees(@RequestParam String companyName){
        return companyService.getEmployees(companyName);
    }
}
