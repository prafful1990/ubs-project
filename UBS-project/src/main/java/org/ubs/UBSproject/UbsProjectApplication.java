package org.ubs.UBSproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UbsProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(UbsProjectApplication.class, args);
	}

}
