package ubs.dao;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.ubs.dao.EmployeeDaoImpl;
import org.ubs.dao.EmployeeRepository;
import org.ubs.model.EmployeeDetails;
import org.ubs.service.EmployeeServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@ExtendWith(MockitoExtension.class)
public class EmployeeDaoImplTest {

    @InjectMocks
    private EmployeeDaoImpl dao;

    @Mock
    private EmployeeRepository repository;

    @Test
    public void testGetEmployees() {
        String companyName = "ABC";
        List<EmployeeDetails> employees = new ArrayList<>();
        EmployeeDetails employee = Mockito.mock(EmployeeDetails.class);
        employees.add(employee);
        Mockito.when(repository.findByCompanyName(companyName)).thenReturn(employees);
        List<EmployeeDetails> response = dao.getEmployees(companyName);
        assertNotNull(response);
        assertFalse(response.isEmpty());
    }
}
