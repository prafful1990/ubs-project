package ubs.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.ubs.dao.EmployeeDaoImpl;
import org.ubs.model.EmployeeDetails;
import org.ubs.model.EmployeeResponse;
import org.ubs.service.EmployeeServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@ExtendWith(MockitoExtension.class)
public class EmployeeServiceImplTest {

    @InjectMocks
    private EmployeeServiceImpl service;

    @Mock
    private EmployeeDaoImpl dao;

    @Test
    public void testGetEmployees() {
        String companyName = "ABC";
        List<EmployeeDetails> employees = new ArrayList<>();
        EmployeeDetails employee = Mockito.mock(EmployeeDetails.class);
        employees.add(employee);
        Mockito.when(dao.getEmployees(companyName)).thenReturn(employees);
        List<EmployeeDetails> response = service.getEmployees(companyName);
        assertNotNull(response);
        assertFalse(response.isEmpty());
    }
}
