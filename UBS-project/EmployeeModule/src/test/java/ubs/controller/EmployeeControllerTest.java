package ubs.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.ubs.controller.EmployeeController;
import org.ubs.model.EmployeeDetails;
import org.ubs.model.EmployeeResponse;
import org.ubs.service.EmployeeService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class EmployeeControllerTest {

    @InjectMocks
    private EmployeeController controller;

    @Mock
    private EmployeeService service;

    @Test
    public void testGetEmployees() {
        String companyName = "ABC";
        List<EmployeeDetails> employees = new ArrayList<>();
        EmployeeDetails employee = Mockito.mock(EmployeeDetails.class);
        employees.add(employee);
        Mockito.when(service.getEmployees(companyName)).thenReturn(employees);
        EmployeeResponse response = controller.findEmployee(companyName);
        assertNotNull(response);
        assertFalse(response.getEmployees().isEmpty());
    }
}
