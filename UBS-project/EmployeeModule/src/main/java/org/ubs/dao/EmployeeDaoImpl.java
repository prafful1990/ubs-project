package org.ubs.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.ubs.model.EmployeeDetails;

import java.util.List;

@Repository
public class EmployeeDaoImpl implements EmployeeDao{

    @Autowired
    private EmployeeRepository repository;

    @Override
    public List<EmployeeDetails> getEmployees(String companyName) {
        return repository.findByCompanyName(companyName);
    }
}
