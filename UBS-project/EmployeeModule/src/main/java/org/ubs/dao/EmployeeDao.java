package org.ubs.dao;

import org.ubs.model.EmployeeDetails;

import java.util.List;

public interface EmployeeDao {
    List<EmployeeDetails> getEmployees(String companyName);
}
