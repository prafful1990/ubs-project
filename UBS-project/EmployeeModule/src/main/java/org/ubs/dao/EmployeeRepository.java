package org.ubs.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.ubs.model.EmployeeDetails;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<EmployeeDetails, String> {

    List<EmployeeDetails> findByCompanyName(String companyName);
}
