package org.ubs.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "EMPLOYEES")
@Getter
@Setter
public class EmployeeDetails {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String name;
    private String position;
    private String salary;
    @Column(name = "COMPANY_NAME")
    private String companyName;
}
