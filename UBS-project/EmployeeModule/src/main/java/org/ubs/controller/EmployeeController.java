package org.ubs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.ubs.model.EmployeeResponse;
import org.ubs.service.EmployeeService;

@RestController
@RequestMapping("employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping("details")
    public EmployeeResponse findEmployee(@RequestParam String companyName){
        EmployeeResponse response = new EmployeeResponse();
        response.setEmployees(employeeService.getEmployees(companyName));
        return response;
    }
}
