package org.ubs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ubs.dao.EmployeeDao;
import org.ubs.model.EmployeeDetails;
import org.ubs.model.EmployeeResponse;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeDao employeeDao;

    @Override
    public List<EmployeeDetails> getEmployees(String companyName) {
        return employeeDao.getEmployees(companyName);
    }
}
